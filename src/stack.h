/**
 * @file stack.h
 * @author Aurelien Pasquet (aurelien.pasquet@etu.u-bordeaux.fr)
 * @brief Basic Stack Functions
 * 
 * @copyright Copyright (c) 2023
 * 
 */

#ifndef __STACK_H__
#define __STACK_H__

#include <stdbool.h>

#include "maze.h"

/**
 * @brief Standard unsigned integer type.
 */
typedef unsigned int uint;

/**
 * @brief The structure pointer that stores the element of the stack
 */
typedef struct
{
    tile *array;
    int n;
    int nmax;
} *stack;

/**
 * @brief Creates a new empty stack
 * 
 * @param nmax size of the stack
 * @return stack 
 */
stack create_stack(int nmax);

/**
 * @brief Frees the stack
 * 
 * @param s stack
 */
void free_stack(stack s);

/**
 * @brief Checks if the stack is empty
 * 
 * @param stack stack
 * @pre @p stack must be a valid pointer toward a stack structure.
 * @return true if stack is empty -
 * @return false if not
 */
bool is_empty_stack(stack s);

/**
 * @brief Pushes a new tile at the top of the stack
 * 
 * @param s stack
 * @param t tile to push
 * @pre @p stack must be a valid pointer toward a stack structure.
 */
void push_stack(stack s, tile t);

/**
 * @brief Pops the element at the top of the stack and returns its value
 * 
 * @param stack stack
 * @pre @p stack must be a valid pointer toward a stack structure.
 * @return tile at the top of the stack 
 */
tile pop_stack(stack s);

/**
 * @brief Returns the value of the element at the top of the stack
 * 
 * @param stack stack
 * @pre @p stack must be a valid pointer toward a stack structure.
 * @return tile at the top of the stack
 */
tile top_stack(stack s);

#endif