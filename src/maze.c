#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "maze.h"
#include "stack.h"
#include "tools.h"

struct maze_s 
{   
    uint rows;
    uint cols;
    tile **tiles;
};

/*----------------------------------------------------------------------------------------------*/

maze create_maze(uint width, uint height, uint tile_size)
{   
    uint rows = height / tile_size;
    uint cols = width / tile_size;

    maze maze = malloc(sizeof(maze) + sizeof(tile**));
    assert(maze);

    tile **tiles = (tile**) malloc(rows*sizeof(tile*));
    assert(tiles);

    for (uint i = 0; i < rows; i++)
    {
        tiles[i] = (tile*) malloc(cols*sizeof(tile));
        assert(tiles[i]);
    }

    for (uint i = 0; i < rows; i++)
    {
        for (uint j = 0; j < cols; j++)
        {   
            uint x = j*tile_size;
            uint y = i*tile_size;

            tile new_tile;
            new_tile.x = x;
            new_tile.y = y;
            new_tile.i = i;
            new_tile.j = j;
            new_tile.size = tile_size;
            new_tile.visited = false;
            new_tile.wall_n = true;
            new_tile.wall_s = true;
            new_tile.wall_e = true;
            new_tile.wall_w = true;

            texture texture = WALL;
            if (x == 0 && y == 0) texture = NW_CORNER;
            else if (x == 0 && y == height - tile_size) texture = SW_CORNER;
            else if (x == width - tile_size && y == 0) texture = NE_CORNER;
            else if (x == width - tile_size && y == height - tile_size) texture = SE_CORNER;
            else if (x == 0) texture = W_BORDER;
            else if (x == width - tile_size) texture = E_BORDER;
            else if (y == 0) texture = N_BORDER;
            else if (y == height - tile_size) texture = S_BORDER;

            new_tile.texture = texture;

            tiles[i][j] = new_tile;
        }
    }

    maze->rows = rows;
    maze->cols = cols;
    maze->tiles = tiles;
    return maze;
}

/*----------------------------------------------------------------------------------------------*/

void free_maze(maze maze)
{
    if (maze == NULL) return;
    if (maze->tiles)
    {
        for (uint i = 0; i < maze->rows; i++)
        {
            free(maze->tiles[i]);
        }
        free(maze->tiles);
    }
    free(maze);
}

/*----------------------------------------------------------------------------------------------*/

tile maze_get_tile(maze maze, uint i, uint j)
{
    assert(maze);
    return maze->tiles[i][j];
}

/*----------------------------------------------------------------------------------------------*/

uint maze_get_rows(maze maze)
{
    assert(maze);
    return maze->rows;
}

/*----------------------------------------------------------------------------------------------*/

uint maze_get_cols(maze maze)
{
    assert(maze);
    return maze->cols;
}

/*----------------------------------------------------------------------------------------------*/

texture maze_get_texture(maze maze, uint i, uint j)
{
    assert(maze);
    return maze->tiles[i][j].texture;
}

/*----------------------------------------------------------------------------------------------*/

bool maze_is_visited(maze maze, uint i, uint j)
{
    assert(maze);
    return maze->tiles[i][j].visited;
}

/*----------------------------------------------------------------------------------------------*/

bool maze_has_n_wall(maze maze, uint i, uint j)
{
    assert(maze);
    return maze->tiles[i][j].wall_n;
}

/*----------------------------------------------------------------------------------------------*/

bool maze_has_s_wall(maze maze, uint i, uint j)
{
    assert(maze);
    return maze->tiles[i][j].wall_s;
}

/*----------------------------------------------------------------------------------------------*/

bool maze_has_e_wall(maze maze, uint i, uint j)
{
    assert(maze);
    return maze->tiles[i][j].wall_e;
}

/*----------------------------------------------------------------------------------------------*/

bool maze_has_w_wall(maze maze, uint i, uint j)
{
    assert(maze);
    return maze->tiles[i][j].wall_w;
}

/*----------------------------------------------------------------------------------------------*/

void maze_set_texture(maze maze, uint i, uint j, texture texture)
{
    assert(maze);
    maze->tiles[i][j].texture = texture;
}

/*----------------------------------------------------------------------------------------------*/

void maze_visit_tile(maze maze, uint i, uint j)
{
    assert(maze);
    maze->tiles[i][j].visited = true;
}

/*----------------------------------------------------------------------------------------------*/

void maze_set_n_wall(maze maze, uint i, uint j, bool n_wall)
{
    assert(maze);
    maze->tiles[i][j].wall_n = n_wall;
}

/*----------------------------------------------------------------------------------------------*/

void maze_set_s_wall(maze maze, uint i, uint j, bool s_wall)
{
    assert(maze);
    maze->tiles[i][j].wall_s = s_wall;
}

/*----------------------------------------------------------------------------------------------*/

void maze_set_e_wall(maze maze, uint i, uint j, bool e_wall)
{
    assert(maze);
    maze->tiles[i][j].wall_e = e_wall;
}

/*----------------------------------------------------------------------------------------------*/

void maze_set_w_wall(maze maze, uint i, uint j, bool w_wall)
{
    assert(maze);
    maze->tiles[i][j].wall_w = w_wall;
}

/*----------------------------------------------------------------------------------------------*/

index maze_get_next_tile(maze maze, uint i, uint j, direction dir)
{   
    assert(maze);

    int next_i = i;
    int next_j = j;

    if (dir == UP) next_i -= 1;
    if (dir == DOWN) next_i += 1;
    if (dir == LEFT) next_j -= 1;
    if (dir == RIGHT) next_j += 1;

    index next_index = {next_i, next_j};

    return next_index;
}

/*----------------------------------------------------------------------------------------------*/

int get_nb_neighbors(maze maze, uint i, uint j)
{   
    assert(maze);

    texture tex = maze->tiles[i][j].texture;

    if (tex == NW_CORNER || tex == NE_CORNER || tex == SW_CORNER || tex == SE_CORNER) return 2;
    else if (tex == N_BORDER || tex == S_BORDER || tex == E_BORDER || tex == W_BORDER) return 3;
    else return 4;
}

/*----------------------------------------------------------------------------------------------*/

direction pick_unvisited_neighbor(maze maze, uint i, uint j)
{
    assert(maze);
    
    tile t = maze_get_tile(maze, i, j);
    int nb_neighbors = get_nb_neighbors(maze, i, j);
    
    direction dir[nb_neighbors];

    if (t.texture == NW_CORNER)
    {
        dir[0] = DOWN;
        dir[1] = RIGHT;
    }
    else if (t.texture == NE_CORNER)
    {
        dir[0] = DOWN;
        dir[1] = LEFT;
    }
    else if (t.texture == SW_CORNER)
    {
        dir[0] = UP;
        dir[1] = RIGHT;
    }
    else if (t.texture == SE_CORNER)
    {
        dir[0] = UP;
        dir[1] = LEFT;
    }
    else if (t.texture == N_BORDER)
    {
        dir[0] = DOWN;
        dir[1] = LEFT;
        dir[2] = RIGHT;
    }
    else if (t.texture == S_BORDER)
    {
        dir[0] = UP;
        dir[1] = LEFT;
        dir[2] = RIGHT;
    }
    else if (t.texture == E_BORDER)
    {
        dir[0] = UP;
        dir[1] = DOWN;
        dir[2] = LEFT;
    }
    else if (t.texture == W_BORDER)
    {
        dir[0] = UP;
        dir[1] = DOWN;
        dir[2] = RIGHT;
    }
    else if (t.texture == WALL)
    {
        dir[0] = UP;
        dir[1] = DOWN;
        dir[2] = LEFT;
        dir[3] = RIGHT;
    }

    int nb_unvisited_neighbors = 0;

    for (uint k = 0; k < nb_neighbors; k++)
    {   
        index neighbor_index =  maze_get_next_tile(maze, i, j, dir[k]);
        if (!maze_is_visited(maze, neighbor_index.i, neighbor_index.j)) nb_unvisited_neighbors++;
    }

    if (nb_unvisited_neighbors == 0) return NONE;

    direction unvisited_neighbors[nb_unvisited_neighbors];

    for (uint k = 0, i_un = 0; k < nb_neighbors; k++)
    {
        index neighbor_index = maze_get_next_tile(maze, i, j, dir[k]);

        if (!maze_is_visited(maze, neighbor_index.i, neighbor_index.j))
        {
            unvisited_neighbors[i_un] = dir[k];
            i_un++;
        }
    }
    
    return unvisited_neighbors[randint(0, nb_unvisited_neighbors-1)];
}

/*----------------------------------------------------------------------------------------------*/

void break_walls(maze maze, uint i1, uint j1, uint i2, uint j2)
{
    assert(maze);

    if (i1 > i2 && j1 == j2)
    {
        maze_set_n_wall(maze, i1, j1, false);
        maze_set_s_wall(maze, i2, j2, false);
    }
    else if (i1 == i2 && j1 < j2)
    {
        maze_set_e_wall(maze, i1, j1, false);
        maze_set_w_wall(maze, i2, j2, false);
    }
    else if (i1 < i2 && j1 == j2)
    {
        maze_set_s_wall(maze, i1, j1, false);
        maze_set_n_wall(maze, i2, j2, false);
    }
    else if (i1 == i2 && j1 > j2)
    {
        maze_set_w_wall(maze, i1, j1, false);
        maze_set_e_wall(maze, i2, j2, false);
    }
}

/*----------------------------------------------------------------------------------------------*/

void generate_maze(maze maze)
{      
    assert(maze);

    //RESET MAZE
    for (uint i = 0; i < maze->rows; i++)
    {
        for (uint j = 0; j < maze->cols; j++)
        {   
            tile t = maze_get_tile(maze, i, j);

            int width = maze->cols * t.size;
            int height = maze->rows * t.size;

            texture texture = WALL;
            if (t.x == 0 && t.y == 0) texture = NW_CORNER;
            else if (t.x == 0 && t.y == height - t.size) texture = SW_CORNER;
            else if (t.x == width - t.size && t.y == 0) texture = NE_CORNER;
            else if (t.x == width - t.size && t.y == height - t.size) texture = SE_CORNER;
            else if (t.x == 0) texture = W_BORDER;
            else if (t.x == width - t.size) texture = E_BORDER;
            else if (t.y == 0) texture = N_BORDER;
            else if (t.y == height - t.size) texture = S_BORDER;

            maze->tiles[i][j].texture = texture;
            maze->tiles[i][j].visited = false;
            maze->tiles[i][j].wall_n = true;
            maze->tiles[i][j].wall_s = true;
            maze->tiles[i][j].wall_e = true;
            maze->tiles[i][j].wall_w = true;
        }
    }

    stack visited_tiles = create_stack(maze->rows*maze->cols);

    int i = randint(0, maze->rows-1);
    int j = randint(0, maze->cols-1);

    tile first_tile = maze_get_tile(maze, i, j);

    push_stack(visited_tiles, first_tile);

    while (!is_empty_stack(visited_tiles))
    {   
        tile current_tile = top_stack(visited_tiles);
        uint c_i = current_tile.i;
        uint c_j = current_tile.j;

        if (!maze_is_visited(maze, c_i, c_j))
            maze_visit_tile(maze, c_i, c_j);

        direction next_dir = pick_unvisited_neighbor(maze, c_i, c_j);

        if (next_dir == NONE)
        {   
            maze_set_texture(maze, c_i, c_j, EMPTY);
            pop_stack(visited_tiles);
            continue;
        }
        
        index next_index = maze_get_next_tile(maze, c_i, c_j, next_dir);
        uint n_i = next_index.i;
        uint n_j = next_index.j;
        tile next_tile = maze_get_tile(maze, n_i, n_j);

        break_walls(maze, c_i, c_j, n_i, n_j);

        push_stack(visited_tiles, next_tile);
    }
    free_stack(visited_tiles);
}