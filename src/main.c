/*
Compilation :
    Windows
    > gcc src/*.c -o bin/maze -I include -L lib -lmingw32 -lSDL2main -lSDL2
    > gcc src/*.c -o bin/maze -I include -L lib -lmingw32 -lSDL2main -lSDL2 -mwindows
*/

#include <SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "maze.h"

#define WINDOW_WIDTH 1000
#define WINDOW_HEIGHT 1000
#define TILE_SIZE 50
#define FPS 60

void ExitWithError(const char *message);
void SDL_ExitWithError(const char *message);
void SDL_ExitWindowAndRenderer(SDL_Window *window, SDL_Renderer *renderer, const char *message);
void render_maze(SDL_Window *window, SDL_Renderer *renderer, maze maze);

int main(int argc, char *argv[])
{   
    const float FPS_LIMIT = 1000.0/(float)FPS;

    int window_width = WINDOW_WIDTH;
    int window_height = WINDOW_HEIGHT;
    int tile_size = TILE_SIZE;

    if (argc > 1) window_width = strtol(argv[1], NULL, 10);
    if (argc > 2) window_height = strtol(argv[2], NULL, 10);
    if (argc == 4) tile_size = strtol(argv[3], NULL, 10);
    if (argc > 4) ExitWithError("Invalid number of arguments.");

    if (window_width % tile_size != 0 || window_height % tile_size != 0)
        ExitWithError("Incompatible sizes of window and tiles.");

    if (window_width == tile_size || window_height == tile_size)
        ExitWithError("Window width and height must be greater than tile size.");

    if (tile_size == 1)
        ExitWithError("Tile size must be greater than 1.");

    SDL_Window *window = NULL;
    SDL_Renderer *renderer = NULL;

    //Lancement SDL
    if(SDL_Init(SDL_INIT_VIDEO) != 0) 
        SDL_ExitWithError("SDL initialisation.");

    //Création fenêtre + rendu
    if (SDL_CreateWindowAndRenderer(window_width, window_height, 0, &window, &renderer) != 0) 
        SDL_ExitWithError("Impossible to create window and render.");

    SDL_SetWindowTitle(window, "Maze");

    SDL_Surface *icon = NULL;
    icon = SDL_LoadBMP("src/maze.bmp");

    if (icon == NULL)
        SDL_ExitWindowAndRenderer(window, renderer, "Impossible to load the icon");

    SDL_SetWindowIcon(window, icon);

    /*-------------------------------------------------------------------------------------*/

    SDL_bool run = SDL_TRUE;

    maze maze = create_maze(window_width, window_height, tile_size);

    generate_maze(maze);

    while (run)
    {   
        int time_start = SDL_GetTicks();

        //Do Event Loop
        SDL_Event event;

        while(SDL_PollEvent(&event))
        {   
            switch(event.type)
            {   
                case SDL_KEYDOWN:
                    switch (event.key.keysym.sym)
                    {   
                        case SDLK_r:
                            generate_maze(maze);
                            continue;

                        case SDLK_q:
                            run = SDL_FALSE;
                            continue;;

                        default:
                            continue;
                    }

                case SDL_QUIT:
                    run = SDL_FALSE;
                    break;

                default:
                    continue;
            }
        }
        
        //Do Rendering Loop
        render_maze(window, renderer, maze);
        SDL_RenderPresent(renderer);

        //FPS Cap
        int time_end = SDL_GetTicks();
        float time_elapsed_ms = (float) ((time_end - time_start) / SDL_GetPerformanceFrequency() * 1000.0f);
        SDL_Delay(floor(FPS_LIMIT - time_elapsed_ms));
    }   

    /*-------------------------------------------------------------------------------------*/

    free_maze(maze);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return EXIT_SUCCESS;
}