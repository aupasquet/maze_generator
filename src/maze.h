/**
 * @file maze.h
 * @author Aurelien Pasquet (aurelien.pasquet@etu.u-bordeaux.fr)
 * @brief Basic Maze Functions
 * 
 * @copyright Copyright (c) 2023
 * 
 */

#ifndef __MAZE_H__
#define __MAZE_H__

#include <stdbool.h>
#include <SDL.h>

/**
 * @brief Standard unsigned integer type.
 */
typedef unsigned int uint;

/**
 * @brief The different directions
 */
typedef enum {
    UP,
    DOWN,
    LEFT,
    RIGHT,
    NONE
} direction;

/**
 * @brief The different tile textures in a maze
 */
typedef enum {
    EMPTY,
    WALL,
    N_BORDER,
    S_BORDER,
    E_BORDER,
    W_BORDER,
    NW_CORNER,
    NE_CORNER,
    SW_CORNER,
    SE_CORNER,
} texture;

/**
 * @brief The index of a tile
 */
typedef struct {
    int i, j;
} index;

/**
 * @brief The tile stored in a maze
 */
typedef struct
{
    uint x, y;
    uint i, j;
    uint size;
    texture texture;
    bool visited;
    bool wall_n;
    bool wall_s;
    bool wall_e;
    bool wall_w;
} tile;

/**
 * @brief The structure pointer that stores the maze
 */
typedef struct maze_s *maze;

/**
 * @brief Creates a new empty maze
 * 
 * @param width width of the window
 * @param height height of the window
 * @param tile_size size of the tiles
 * @return the created maze 
 */
maze create_maze(uint width, uint height, uint tile_size);

/**
 * @brief Deletes the maze and frees the allocated memory
 * 
 * @param maze maze to delete
 * @pre @p maze must be a valid pointer toward a maze structure.
 */
void free_maze(maze maze);

/**
 * @brief Gets the tile at coordinates (i, j) in tiles
 * 
 * @param maze maze
 * @param i row index
 * @param j column index
 * @pre @p maze must be a valid pointer toward a maze structure.
 * @pre @p i < maze rows
 * @pre @p j < maze columns
 * @return the tile
 */
tile maze_get_tile(maze maze, uint i, uint j);

/**
 * @brief Gets the number of rows
 * 
 * @param maze maze
 * @pre @p maze must be a valid pointer toward a maze structure
 * @return the number of rows in this maze 
 */
uint maze_get_rows(maze maze);

/**
 * @brief Gets the number of columns
 * 
 * @param maze maze
 * @pre @p maze must be a valid pointer toward a maze structure
 * @return the number of columns in this maze 
 */
uint maze_get_cols(maze maze);

/**
 * @brief Returns the texture of the tile
 * 
 * @param maze 
 * @param i 
 * @param j 
 * @pre @p maze must be a valid pointer toward a maze structure
 * @pre @p i < maze rows
 * @pre @p j < maze columns
 * @return texture 
 */
texture maze_get_texture(maze maze, uint i, uint j);

/**
 * @brief Returns if the tile is visited
 * 
 * @param maze 
 * @param i 
 * @param j 
 * @pre @p maze must be a valid pointer toward a maze structure
 * @pre @p i < maze rows
 * @pre @p j < maze columns
 * @return true 
 * @return false 
 */
bool maze_is_visited(maze maze, uint i, uint j);

/**
 * @brief Returns if the tile has a northern wall
 * 
 * @param maze 
 * @param i 
 * @param j 
 * @pre @p maze must be a valid pointer toward a maze structure
 * @pre @p i < maze rows
 * @pre @p j < maze columns
 * @return true 
 * @return false 
 */
bool maze_has_n_wall(maze maze, uint i, uint j);

/**
 * @brief Returns if the tile has a southern wall
 * 
 * @param maze 
 * @param i 
 * @param j 
 * @pre @p maze must be a valid pointer toward a maze structure
 * @pre @p i < maze rows
 * @pre @p j < maze columns
 * @return true 
 * @return false 
 */
bool maze_has_s_wall(maze maze, uint i, uint j);

/**
 * @brief Returns if the tile has an eastern wall
 * 
 * @param maze 
 * @param i 
 * @param j 
 * @pre @p maze must be a valid pointer toward a maze structure
 * @pre @p i < maze rows
 * @pre @p j < maze columns
 * @return true 
 * @return false 
 */
bool maze_has_e_wall(maze maze, uint i, uint j);

/**
 * @brief Returns if the tile has a western wall
 * 
 * @param maze 
 * @param i 
 * @param j 
 * @pre @p maze must be a valid pointer toward a maze structure
 * @pre @p i < maze rows
 * @pre @p j < maze columns
 * @return true 
 * @return false 
 */
bool maze_has_w_wall(maze maze, uint i, uint j);

/**
 * @brief Sets the texture of the tile
 * 
 * @param maze 
 * @param i 
 * @param j 
 * @pre @p maze must be a valid pointer toward a maze structure
 * @pre @p i < maze rows
 * @pre @p j < maze columns
 * @param texture 
 */
void maze_set_texture(maze maze, uint i, uint j, texture texture);

/**
 * @brief Sets the tile to visited
 * 
 * @param maze 
 * @param i 
 * @param j 
 * @pre @p maze must be a valid pointer toward a maze structure
 * @pre @p i < maze rows
 * @pre @p j < maze columns
 * @param visited 
 */
void maze_visit_tile(maze maze, uint i, uint j);

/**
 * @brief Sets the tile northern wall to n_wall
 * 
 * @param maze 
 * @param i 
 * @param j 
 * @pre @p maze must be a valid pointer toward a maze structure
 * @pre @p i < maze rows
 * @pre @p j < maze columns
 * @param n_wall 
 */
void maze_set_n_wall(maze maze, uint i, uint j, bool n_wall);

/**
 * @brief Sets the tile southern wall to s_wall
 * 
 * @param maze 
 * @param i 
 * @param j 
 * @pre @p maze must be a valid pointer toward a maze structure
 * @pre @p i < maze rows
 * @pre @p j < maze columns
 * @param n_wall 
 */
void maze_set_s_wall(maze maze, uint i, uint j, bool s_wall);

/**
 * @brief Sets the tile estern wall to e_wall
 * 
 * @param maze 
 * @param i 
 * @param j 
 * @pre @p maze must be a valid pointer toward a maze structure
 * @pre @p i < maze rows
 * @pre @p j < maze columns
 * @param n_wall 
 */
void maze_set_e_wall(maze maze, uint i, uint j, bool e_wall);

/**
 * @brief Sets the tile western wall to w_wall
 * 
 * @param maze 
 * @param i 
 * @param j 
 * @pre @p maze must be a valid pointer toward a maze structure
 * @pre @p i < maze rows
 * @pre @p j < maze columns
 * @param n_wall 
 */
void maze_set_w_wall(maze maze, uint i, uint j, bool w_wall);

/**
 * @brief Gets the index of the next tile in the dir direction
 * 
 * @param maze 
 * @param i 
 * @param j 
 * @param dir 
 * @pre @p maze must be a valid pointer toward a maze structure
 * @pre @p i < maze rows
 * @pre @p j < maze columns
 * @return index 
 */
index maze_get_next_tile(maze maze, uint i, uint j, direction dir);

/**
 * @brief Get the number of neighbors of the (i, j) tile
 * 
 * @param maze 
 * @param i 
 * @param j 
 * @pre @p maze must be a valid pointer toward a maze structure
 * @pre @p i < maze rows
 * @pre @p j < maze columns
 * @return int 
 */
int get_nb_neighbors(maze maze, uint i, uint j);

/**
 * @brief Returns the direction of a random unvisited neighbor, NONE if all the neighbors are visited
 * 
 * @param maze 
 * @param i 
 * @param j 
 * @pre @p maze must be a valid pointer toward a maze structure
 * @pre @p i < maze rows
 * @pre @p j < maze columns
 * @return direction 
 */
direction pick_unvisited_neighbor(maze maze, uint i, uint j);

/**
 * @brief Sets the walls between tile (i1, j1) and tile (i2, j2) at false
 * 
 * @param maze 
 * @param i1 
 * @param j1 
 * @param i2 
 * @param j2 
 * @pre @p maze must be a valid pointer toward a maze structure
 * @pre @p i1 < maze rows
 * @pre @p j1 < maze columns
 * @pre @p i2 < maze rows
 * @pre @p j2 < maze columns
 */
void break_walls(maze maze, uint i1, uint j1, uint i2, uint j2);

/**
 * @brief Generates a maze as a connected subgraph of the window
 * 
 * @param maze maze
 * @pre @p maze must be a valid pointer toward a maze structure
 */
void generate_maze(maze maze);

#endif