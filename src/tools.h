/**
 * @file tools.h
 * @author Aurelien Pasquet (aurelien.pasquet@etu.u-bordeaux.fr)
 * @brief Auxiliary Functions
 * 
 * @copyright Copyright (c) 2023
 * 
 */

#ifndef __TOOLS_H__
#define __TOOLS_H__

#include <stdbool.h>
#include <SDL.h>

#include "maze.h"

/**
 * @brief Standard unsigned integer type.
 */
typedef unsigned int uint;

/**
 * @brief Exits programm with an error
 * 
 * @param message error message
 */
void ExitWithError(const char *message);

/**
 * @brief Exits programm and Quits SDL with an error
 * 
 * @param message error message
 */
void SDL_ExitWithError(const char *message);

/**
 * @brief Exits programm and destroys window and renderer
 * 
 * @param window window to destroy
 * @param renderer renderer to destroy
 * @param message error message
 */
void SDL_ExitWindowAndRenderer(SDL_Window *window, SDL_Renderer *renderer, const char *message);

/**
 * @brief Defines the drawing color
 * 
 * @param window window
 * @param renderer renderer
 * @param color array of size 3 : {r, g, b}
 * @param a alpha parameter
 */
void DrawColor(SDL_Window *window, SDL_Renderer *renderer, int color[], Uint8 a);

/**
 * @brief Draws a filled rectangle
 * 
 * @param window window
 * @param renderer renderer
 * @param rect rectangle to draw
 */
void FillRect(SDL_Window *window, SDL_Renderer *renderer, const SDL_Rect *rect);

/**
 * @brief Draws a line from (x1, y1) to (x2, y2)
 * 
 * @param window window
 * @param renderer renderer
 * @param x1 
 * @param y1 
 * @param x2 
 * @param y2 
 */
void DrawLine(SDL_Window *window, SDL_Renderer *renderer, int x1, int y1, int x2, int y2);

/**
 * @brief Returns a randow integer in [x, y]
 * 
 * @param x 
 * @param y 
 * @return random int in [x, y]
 */
int randint(int x, int y);

/**
 * @brief Renders the maze
 * 
 * @param window window
 * @param renderer renderer
 * @param maze maze
 */
void render_maze(SDL_Window *window, SDL_Renderer *renderer, maze maze);

#endif