#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>

#include "tools.h"
#include "maze.h"

void ExitWithError(const char *message)
{   
    printf("ERROR > ");
    fprintf(stderr, message);
    exit(EXIT_FAILURE);
}

void SDL_ExitWithError(const char *message)
{
    SDL_Log("ERROR : %s > %s\n", message, SDL_GetError());
    SDL_Quit();
    exit(EXIT_FAILURE);
}

void SDL_ExitWindowAndRenderer(SDL_Window *window, SDL_Renderer *renderer, const char *message)
{
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_ExitWithError(message);
}

void DrawColor(SDL_Window *window, SDL_Renderer *renderer, int color[], Uint8 a)
{   
    assert(window);
    assert(renderer);
    if (SDL_SetRenderDrawColor(renderer, color[0], color[1], color[2], SDL_ALPHA_OPAQUE) != 0)
        SDL_ExitWindowAndRenderer(window, renderer, "Impossible to change render color"); 
}

void FillRect(SDL_Window *window, SDL_Renderer *renderer, const SDL_Rect *rect)
{
    assert(window);
    assert(renderer);
    if (SDL_RenderFillRect(renderer, rect) != 0)
        SDL_ExitWindowAndRenderer(window, renderer, "Impossible to draw rectangle");
}

void DrawLine(SDL_Window *window, SDL_Renderer *renderer, int x1, int y1, int x2, int y2)
{
    assert(window);
    assert(renderer);
    if (SDL_RenderDrawLine(renderer, x1, y1, x2, y2) != 0)
        SDL_ExitWindowAndRenderer(window, renderer, "Impossible to draw line");
}

int randint(int x, int y)
{   
    if (y < x)
    {
        fprintf(stderr, "ERROR > (y < x) in randint");
        exit(EXIT_FAILURE);
    }
    return x + rand()%(y-x+1);
}

void render_maze(SDL_Window *window, SDL_Renderer *renderer, maze maze)
{   
    //TILES
    for (uint i = 0; i < maze_get_rows(maze); i++)
    {
        for (uint j = 0; j < maze_get_cols(maze); j++)
        {   
            tile tile = maze_get_tile(maze, i, j);

            //Color
            int color[] = {0, 0, 0};
            if (maze_get_texture(maze, i, j) == EMPTY)
            {   
                for (uint i = 0; i < 3; i++) color[i] = 255;
            }
            DrawColor(window, renderer, color, SDL_ALPHA_OPAQUE);

            //Rectangle
            SDL_Rect tile_rect;

            tile_rect.x = tile.x;
            tile_rect.y = tile.y;
            tile_rect.w = tile.size;
            tile_rect.h = tile.size;

            FillRect(window, renderer, &tile_rect);
        }
    }

    //WALLS
    for (uint i = 0; i < maze_get_rows(maze); i++)
    {
        for (uint j = 0; j < maze_get_cols(maze); j++)
        {
            tile tile = maze_get_tile(maze, i, j);

            int color[] = {0, 0, 0};
            DrawColor(window, renderer, color, SDL_ALPHA_OPAQUE);

            if (tile.wall_n) DrawLine(window, renderer, tile.x, tile.y, tile.x + tile.size, tile.y);
            if (tile.wall_s) DrawLine(window, renderer, tile.x, tile.y + tile.size, tile.x + tile.size, tile.y + tile.size);
            if (tile.wall_e) DrawLine(window, renderer, tile.x + tile.size, tile.y, tile.x + tile.size, tile.y + tile.size);
            if (tile.wall_w) DrawLine(window, renderer, tile.x, tile.y, tile.x, tile.y + tile.size);
        }
    }
}