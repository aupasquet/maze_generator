#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "stack.h"
#include "maze.h"

stack create_stack(int nmax)
{
    stack s = malloc(sizeof(*s));
    assert(s);

    s->array = malloc(nmax*sizeof(tile));
    assert(s->array);

    s->n = 0;
    s->nmax = nmax;

    return s;
}

void free_stack(stack s)
{
    if (s != NULL) free(s->array);
    free(s);
}

bool is_empty_stack(stack s)
{
    return (s->n == 0);
}

void push_stack(stack s, tile t)
{
    if (s->n == s->nmax)
    {
        s->nmax *= 2;
        s->array = realloc(s->array, s->nmax*sizeof(tile));
    }

    s->array[s->n] = t;
    s->n++;
}

tile pop_stack(stack s)
{
    if (s->n == 0)
    {
        fprintf(stderr, "ERROR > Cannot pop element out of empty stack");
        exit(EXIT_FAILURE);
    }

    tile t = s->array[s->n-1];
    s->n--;
    return t;
}

tile top_stack(stack s)
{
    if (s->n == 0)
    {
        fprintf(stderr, "ERROR > Cannot return top element of an empty stack");
        exit(EXIT_FAILURE);
    }

    return s->array[s->n-1];
}